<?php

use Entity\Link;

/**
 * @var Link[] $models
 */
?>

<h1>list of links</h1>

<ul>
    <?php foreach ($models as $model): ?>

        <li>
            <a href="page-show?id=<?= $model->getId() ?>"><?= $model->getLink() ?></a>
        </li>

    <?php endforeach; ?>

</ul>
