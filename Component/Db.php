<?php
namespace Component;
use PDO;

/**
 * Class Db
 */
class Db
{
    /**
     * @var string
     */
    private $dsn;
    /**
     * @var string
     */
    private $username;
    /**
     * @var string
     */
    private $password;

    /**
     * Db constructor.
     *
     * @param string $dsn
     * @param string $username
     * @param string $password
     */
    public function __construct(string $dsn,string $username,string $password)
    {
        $this->dsn = $dsn;
        $this->username = $username;
        $this->password = $password;
    }

    /**
     * @return PDO
     */
    private function createPDO() : PDO {
         return  new PDO($this->dsn,$this->username,$this->password);
    }

    /**
     * @param string $sql
     *
     * @param array $param
     *
     * @return array
     */
    public function query(string $sql,array $param =[]) : array {
        $db =  $this->createPDO();
        $query =$db->prepare($sql);
        $query->execute($param);
        return $query->fetchAll();
    }
}