<?php
namespace Component;
use Exception\Exception;
use Exception\NotFoundException;

/**
 * Class App
 */
class App
{
    /**
     * @var Request
     */
    private $request;

    /**
     * @var array
     */
    private $gets = [];

    /**
     * App constructor.
     *
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * @param string $pattern
     * @param callable $callable
     *
     * @return void
     */
    public function get(string  $pattern,callable $callable) : void {
        $this->gets[$pattern] = $callable;
    }

    /**
     * @throws NotFoundException
     *
     * @return void
     */
    public function run() : void {
        if($this->request->isGet()) {
            foreach ($this->gets as $pattern=>$callback) {
                if($pattern == $this->request->path()) {
                    $callback($this->request);
                    return;
                }
            }
        }
        throw new NotFoundException();
    }
}