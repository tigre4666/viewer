<?php

namespace Component;

use Exception\NotFountViewException;

/**
 * Class Renderer
 */
class Renderer
{
    /**
     * @var string
     */
    private $rootFolder;

    /**
     * @var string
     */
    private $layout;

    /**
     * Renderer constructor.
     *
     * @param string $rootFolder
     * @param string $layout
     */
    public function __construct(string $rootFolder, string $layout = 'layout')
    {
        $this->rootFolder = $rootFolder;
        $this->layout = $layout;
    }

    /**
     * @param string $view
     *
     * @return string
     */
    private function getPathView(string $view): string
    {
        return $this->rootFolder . '/' . $view . '.php';
    }

    /**
     * @param string $view
     * @param array $data
     *
     * @return string
     *
     * @throws NotFountViewException
     */
    public function render(string $view, $data = []): string
    {
        $content = $this->partial($view, $data);
        $data['content'] = $content;
        return $this->partial($this->layout,$data);
    }

    /**
     * @param string $view
     * @param array $data
     * @return string
     *
     * @throws NotFountViewException
     */
    public function partial(string $view, $data = []): string
    {
        $viewPath = $this->getPathView($view);
        if (file_exists($viewPath)) {
            ob_start();
            extract($data);
            require $viewPath;
            $content = ob_get_contents();
            ob_end_clean();
            return $content;
        }
        throw new NotFountViewException();
    }
}