<?php
namespace Component;

/**
 * Class Request
 */
class Request
{
    /**
     * @return bool
     */
    public function isGet() : bool {
        return $_SERVER['REQUEST_METHOD']=='GET';
    }

    /**
     * @return null|string
     */
    public function path() : ?string {
        return $_SERVER['REDIRECT_URL'];
    }

    /**
     * @param string $name
     * @param null $default
     *
     * @return null|mixed
     */
    public function get(string $name,$default= null) {
        if(isset($_GET[$name])) {
            return $_GET[$name];
        }
        return $default;
    }
}