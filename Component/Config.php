<?php

namespace Component;

use Exception\InvalidConfigException;

/**
 * Class Config
 */
class Config
{
    /**
     * @var array
     */
    private $parameters;

    /**
     * Config constructor.
     *
     * @param array $parameters
     */
    public function __construct(array $parameters)
    {
        $this->parameters = $parameters;
    }

    /**
     * @param string $name
     *
     * @return mixed
     *
     * @throws InvalidConfigException
     */
    public function get(string $name)
    {
        if (isset($this->parameters[$name])) {
            return $this->parameters[$name];
        }
        throw new InvalidConfigException();
    }
}