<?php
require_once 'autoload.php';

use Factory\{
    AppFactory,
    RendererFactory,
    LinkRepositoryFactory,
    PageRepositoryFactory
};

use Exception\{
    NotFoundException
};

use lib\{
    PageRenderer
};

use Component\{
    Request
};

$app = AppFactory::factory();
$renderer = RendererFactory::factory();

$app->get('', function () use ($renderer) {
    echo $renderer->render('index', ['tile' => 'Welcome']);
});

$app->get('/links', function () use ($renderer) {
    $repository = LinkRepositoryFactory::factory();
    echo $renderer->render('list-of-links', ['models' => $repository->all()]);

});

$app->get('/page-show', function (Request $request) use ($renderer) {
    $id = $request->get('id');
    $repository = PageRepositoryFactory::factory();
    $model = $repository->getPK($id);
    if ($model == null) {
        throw new NotFoundException();
    }
    $html = new PageRenderer($model);
    echo $renderer->render('page', ['html' => $html, 'title' => $model->getTitle()]);
});

$app->run();
