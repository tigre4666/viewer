<?php

namespace Entity;

/**
 * Class Link
 */
class Link
{
    /**
     * @var int
     */
    private $id;
    /**
     * @var string
     */
    private $link;
    /**
     * @var int
     */
    private $pageId;

    /**
     * Gets the id of the link
     *
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Sets the id of link
     *
     * @param int $id
     *
     * @return Link
     */
    public function setId(int $id) : self
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Gets the link of the link
     *
     * @return null|string
     */
    public function getLink() : ?string
    {
        return $this->link;
    }

    /**
     * Sets the id of link
     *
     * @param int $id
     *
     * @return Link
     */
    public function setLink(string $link) : self
    {
        $this->link = $link;
        return $this;
    }


    /**
     * Gets the link of the link
     *
     * @return null|string
     */
    public function getPageId() : ?int
    {
        return $this->pageId;
    }

    /**
     * Sets the id of link
     *
     * @param int $id
     *
     * @return Link
     */
    public function setPageId(int $pageId) : self
    {
        $this->pageId = $pageId;
        return $this;
    }




}