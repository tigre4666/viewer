<?php

namespace Entity;

/**
 * Class Page
 */
class Page
{

    const MINE_HTML = 'text/html';
    const MINE_TEXT = 'text/plain';

    /**
     * @var int
     */
    private $id;
    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $mime;

    /**
     * @var string
     */
    private $text;


    /**
     * Gets the id of the page
     *
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Sets a id of the page
     *
     * @param int $id
     *
     * @return Page
     */
    public function setId(int $id): self
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Gets the id of the page
     *
     * @return int|null
     */
    public function getText(): ?string
    {
        return $this->text;
    }

    /**
     * Sets a id of the page
     *
     * @param int $id
     *
     * @return Page
     */
    public function setText(string $text): self
    {
        $this->text = $text;
        return $this;
    }

    /**
     * Sets a mine of Page
     *
     * @param string $mine
     *
     * @return $this
     */
    public function setMime(string $mine): self
    {
        $this->mime = $mine;
        return $this;
    }

    /**
     * Gets the mine of the page
     *
     * @return string|null
     */
    public function getMime(): ?string
    {
        return $this->mime;
    }

    /**
     * Sets a mine of Page
     *
     * @param string $title
     *
     * @return $this
     */
    public function setTitle(string $title): self
    {
        $this->title = $title;
        return $this;
    }

    /**
     * Gets the mine of the page
     *
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }
}