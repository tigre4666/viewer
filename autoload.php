<?php

spl_autoload_register(function (string $className) {
    include(__DIR__ . "/" . $className . ".php");
});