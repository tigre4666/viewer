<?php
namespace Factory;


use Repository\LinkRepository;
use Repository\PageRepository;

class PageRepositoryFactory
{
    public static function factory() : PageRepository
    {
        return new PageRepository(DbFactory::factory());
    }
}