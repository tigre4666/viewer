<?php
namespace Factory;


use Component\Renderer;

class RendererFactory
{
    public static function factory() :Renderer {
        return new Renderer(__DIR__.'/../views');
    }
}