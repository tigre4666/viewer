<?php
namespace Factory;

use Component\App;
use Component\Request;

class AppFactory
{
    public static function factory() : App
    {
       return new App(new Request());
    }
}