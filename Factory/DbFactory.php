<?php

namespace Factory;

use Component\Db;

class DbFactory
{
    public static function factory(): Db
    {
        $config = ConfigFactory::factory();
        $dbConfig = $config->get('db');
        return new Db($dbConfig['dsn'], $dbConfig['username'], $dbConfig['password']);
    }
}