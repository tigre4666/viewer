<?php

namespace Factory;

use Repository\LinkRepository;

class LinkRepositoryFactory
{
    /**
     * @return LinkRepository
     */
    public static function factory(): LinkRepository
    {
        return new LinkRepository(DbFactory::factory());
    }
}