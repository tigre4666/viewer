<?php
namespace Factory;

use Component\Config;

class ConfigFactory
{
    public static function factory() : Config {
        $config = require_once __DIR__.'/../config.php';
        return new Config($config);
    }
}