<?php
/**
 * Created by PhpStorm.
 * User: bond
 * Date: 3/14/2018
 * Time: 12:38 PM
 */

namespace lib;


use Entity\Page;
use Exception\MimeUnsupportedException;
use Lib\Factory\TextToHtmlFactory;
use lib\Сonverter\ConverterInterface;
use lib\Сonverter\HtmlToHtml;

/**
 * Class PageRenderer
 */
class PageRenderer
{
    /**
     * @var Page
     */
    private $page;
    /**
     * @var ConverterInterface[]
     */
    private $converters;

    public function __construct(Page $page)
    {
        $this->converters = [TextToHtmlFactory::Factory(), new HtmlToHtml()];
        $this->page = $page;
    }

    /**
     * @return string
     *
     * @throws MimeUnsupportedException
     */
    public function renderer(): string
    {
        foreach ($this->converters as $converter) {
            if ($converter->isSupport($this->page->getMime())) {
                return $converter->to($this->page->getText());
            }
        }
        throw new MimeUnsupportedException();

    }

    /**
     * @return string
     *
     * @throws MimeUnsupportedException
     */
    public function __toString()
    {
        return $this->renderer();
    }
}