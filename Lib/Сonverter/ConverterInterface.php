<?php
/**
 * Created by PhpStorm.
 * User: bond
 * Date: 3/15/2018
 * Time: 4:27 PM
 */

namespace lib\Сonverter;


/**
 * Class ConverterInterface
 */
interface ConverterInterface
{
    /**
     * Checks supports a mine
     *
     * @param string $mime
     *
     * @return bool
     */
    function isSupport(string $mime): bool;

    /**
     * @param string $text
     *
     * @return string
     */
    function to(string $text): string;
}