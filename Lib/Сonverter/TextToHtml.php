<?php

namespace lib\Сonverter;

use Entity\Page;
use Lib\Replace\Replace;
use Lib\Tag\H1Tag;

class TextToHtml
{
    /**
     * @var Replace[]
     */
    private $replaces;
    /**
     * @param string $text
     */
    public function __construct(array $replaces)
    {
        $this->replaces = $replaces;
    }

    /**
     * @param string $text
     *
     * @return string
     */
    public function to(string $text){
        $result = [];
        $lines = explode("\r\n", $text);
        $first = array_shift($lines);
        $result[] = new H1Tag($first);
        foreach ($lines as $line) {
            $result[] = $this->replace($line);
        }
        return implode("\n", $result);
    }

    public function isSupport(string $mime) {
        return $mime == Page::MINE_TEXT;
    }

    /**
     * @param string $text
     *
     * @return string
     */
    private function replace(string $text) : string {
        foreach ($this->replaces as $replace){
           $text = $replace->replace($text);
        }
        return $text;
    }
}