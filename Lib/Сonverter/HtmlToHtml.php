<?php
/**
 * Created by PhpStorm.
 * User: bond
 * Date: 3/15/2018
 * Time: 4:26 PM
 */

namespace lib\Сonverter;


use Entity\Page;

class HtmlToHtml implements ConverterInterface
{
    /**
     * @inheritdoc
     */
    public function isSupport(string $mime) : bool {
        return $mime == Page::MINE_HTML;
    }

    /**
     * @inheritdoc
     */
    function to(string $text): string
    {
       return $text;
    }
}