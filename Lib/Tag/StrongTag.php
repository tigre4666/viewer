<?php

namespace Lib\Tag;

/**
 * Class StrongTag
 */
class StrongTag extends Tag
{

    /**
     * @inheritdoc
     */
    protected function tag(): string
    {
        return 'strong';
    }
}