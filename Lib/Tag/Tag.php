<?php

namespace Lib\Tag;

abstract class Tag
{
    /**
     * @var string
     */
    private $content;

    protected abstract function tag() : string ;

    /**
     * H1Tag constructor.
     *
     * @param string|Tag|Tag[] $content
     */
    public function __construct(string $content)
    {
        $this->content = $content;
    }

    /**
     * @return string
     */
    private function openTag() : string {
         return '<'.$this->tag().'>';
    }

    /**
     * @return string
     */
    private function closeTag() : string {
        return '</'.$this->tag().'>';
    }
    /**
     * @return string
     */
    public function __toString()
    {
        $result = $this->openTag();
        if (is_array($this->content)) {
            $result .= implode("\n", $this->content);
        } else {
            $result .= $this->content;
        }
        $result .= $this->closeTag();
        return $result;
    }
}