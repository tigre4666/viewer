<?php

namespace Lib\Tag;

/**
 * Class LiTag
 */
class LiTag extends Tag
{
    /**
     * @inheritdoc
     */
    protected function tag(): string
    {
        return 'li';
    }
}