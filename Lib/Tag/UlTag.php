<?php

namespace Lib\Tag;

/**
 * Class UlTag
 */
class UlTag extends Tag
{
    /**
     * @inheritdoc
     */
    protected function tag(): string
    {
       return 'ul';
    }
}