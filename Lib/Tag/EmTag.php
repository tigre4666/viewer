<?php
namespace Lib\Tag;

/**
 * Class EmTag
 */
class EmTag extends Tag
{
    /**
     * @inheritdoc
     */
    protected function tag(): string
    {
       return 'em';
    }
}