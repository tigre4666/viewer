<?php
namespace Lib\Tag;

class H2Tag extends Tag
{
    /**
     * @inheritdoc
     */
    protected function tag(): string
    {
        return 'H1';
    }
}