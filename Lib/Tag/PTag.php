<?php

namespace Lib\Tag;

/**
 * Class PTag
 */
class PTag extends Tag
{
    /**
     * @inheritdoc
     */
    protected function tag(): string
    {
        return 'p';
    }
}