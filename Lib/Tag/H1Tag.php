<?php

namespace Lib\Tag;

/**
 * Class H1Tag
 */
class H1Tag extends Tag
{

    /**
     * @inheritdoc
     */
    protected function tag(): string
    {
       return 'h1';
    }
}