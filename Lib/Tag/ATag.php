<?php
/**
 * Created by PhpStorm.
 * User: bond
 * Date: 3/15/2018
 * Time: 1:30 PM
 */

namespace Lib\Tag;

/**
 * Class ATag
 */
class ATag extends Tag
{
    /**
     * @inheritdoc
     */
    protected function tag(): string
    {
        return 'a';
    }
}