<?php
namespace Lib\Replace;

use Lib\Tag\EmTag;

/**
 * Class ReplaceEm
 */
class ReplaceEm extends Replace
{

    /**
     * @inheritdoc
     */
    protected function getPattern(): string
    {
        return '#\*(\S.+?)\*#';
    }

    /**
     * @param array $data
     *
     * @return string
     */
    protected function handler(array $data): string
    {
        return new EmTag($data[1]);
    }
}