<?php
namespace Lib\Replace;

use Lib\Tag\PTag;

class ReplaceEmpty extends Replace
{

    /**
     * @return string
     */
    protected function getPattern(): string
    {
        return '#^$#';
    }

    /**
     * @param array $data
     *
     * @return string
     */
    protected function handler(array $data): string
    {
        return new PTag('');
    }
}