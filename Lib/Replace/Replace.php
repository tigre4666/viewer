<?php

namespace Lib\Replace;

abstract class Replace
{
    /**
     * @return string
     */
    protected abstract function getPattern(): string;

    /**
     * @param array $data
     *
     * @return string
     */
    protected abstract function handler(array $data): string;

    /**
     * @param string $text
     *
     * @return string
     */
    public function replace(string $text): string
    {
        return preg_replace_callback($this->getPattern(), [$this, 'handler'], $text);
    }
}