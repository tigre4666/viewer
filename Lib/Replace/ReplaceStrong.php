<?php

namespace Lib\Replace;

use Lib\Tag\StrongTag;

class ReplaceStrong extends Replace
{
    /**
     * @inheritdoc
     */
    protected function getPattern(): string
    {
        return '#\*\*(.+?)\*\*#';
    }

    /**
     * @inheritdoc
     */
    protected function handler(array $data): string
    {
        return new StrongTag($data[1]);
    }
}