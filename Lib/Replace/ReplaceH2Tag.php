<?php

namespace Lib\Replace;

use Lib\Tag\H2Tag;

class ReplaceH2Tag extends Replace
{

    /**
     * @return string
     */
    protected function getPattern(): string
    {
        return '/^##\s(.+)/';
    }

    /**
     * @param array $data
     *
     * @return string
     */
    protected function handler(array $data): string
    {
       return new H2Tag($data[1]);
    }
}