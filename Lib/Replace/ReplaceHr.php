<?php
namespace Lib\Replace;

class ReplaceHr extends Replace
{

    /**
     * @inheritdoc
     */
    protected function getPattern(): string
    {
        return '#^========$#';
    }

    /**
     * @inheritdoc
     */
    protected function handler(array $data): string
    {
        return '<hr />';
    }
}