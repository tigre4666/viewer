<?php
namespace Lib\Factory;

use Lib\Replace\ReplaceEm;
use Lib\Replace\ReplaceEmpty;
use Lib\Replace\ReplaceH2Tag;
use Lib\Replace\ReplaceHr;
use Lib\Replace\ReplaceStrong;
use lib\Сonverter\TextToHtml;

/**
 * Class TextToHtmlFactory
 */
class TextToHtmlFactory
{
    public static function Factory() : TextToHtml {
        return new TextToHtml([
            new ReplaceEmpty(),
            new ReplaceH2Tag(),
            new ReplaceHr(),
            new ReplaceStrong(),
            new ReplaceEm()
        ]);
    }
}