<?php
namespace Repository;

use Component\Db;
use Entity\Page;

class PageRepository
{
    private $db;

    public function __construct(Db $db)
    {
        $this->db = $db;
    }

    public function getPK(int $id) : ?Page {
       $records = $this->db->query('SELECT * FROM page WHERE id=? LIMIT 1',[$id]);
       if($records) {
            return $this->mapper($records[0]);
       }
       return null;
    }

    private function mapper(array $record) {
        $model = new Page();
        $model->setId($record['id']);
        $model->setTitle($record['title']);
        $model->setMime($record['mime']);
        $model->setText($record['text']);
        return $model;
    }
}