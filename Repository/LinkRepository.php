<?php

namespace Repository;

use Component\Db;
use Entity\Link;
use Entity\Page;

/**
 * Class LinkRepositroy
 */
class LinkRepository
{
    private $db;

    public function __construct(Db $db)
    {
        $this->db = $db;
    }

    /**
     * @return Link[]
     */
    public function all(): array
    {
        $result = [];
        foreach ($this->db->query('SELECT * FROM link') as $record) {
            $result[] = $this->mapper($record);
        }
        return $result;

    }

    private function mapper(array $record): Link
    {
        $model = new Link();
        $model->setId($record['id']);
        $model->setPageId($record['page_id']);
        $model->setLink($record['link']);
        return $model;
    }
}